import { Flex, Text } from '@chakra-ui/react'
import LogoSquare from './LogoSquare'

const Footer = () => (
  <Flex alignItems="center" as="footer" py={4}>
    <LogoSquare size="1.5rem" />
    <Text ml={2}>Footer</Text>
  </Flex>
)

export default Footer
