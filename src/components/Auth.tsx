import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { useSessionStorageValue } from '@react-hookz/web'

const Auth = ({ children }: { children: JSX.Element }) => {
  const [walletKey] = useSessionStorageValue('wallet-key')
  const router = useRouter()
  const isRoot = router.pathname === '/'

  useEffect(() => {
    if (!walletKey && !isRoot) {
      router.push('/')
    }
  }, [walletKey, isRoot, router])

  if (walletKey || isRoot) {
    return <>{children}</>
  }

  return null
}

export default Auth
