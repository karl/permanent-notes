import { useCallback } from 'react'
import { useDropzone } from 'react-dropzone'
import { Center, useColorModeValue, Icon } from '@chakra-ui/react'
import { RiFile3Line } from 'react-icons/ri'

type DropzoneProps = {
  readonly dropText: string
  readonly fileType: string
  readonly maxFiles?: number
  readonly multiple?: boolean
  readonly onFileAccepted: (acceptedFile: File[]) => void
}

const Dropzone = ({ dropText, fileType, maxFiles = 1, multiple = false, onFileAccepted }: DropzoneProps) => {
  const onDrop = useCallback((acceptedFiles) => onFileAccepted(acceptedFiles), [onFileAccepted])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: fileType,
    maxFiles,
    multiple,
    onDrop,
  })

  const text = isDragActive ? 'Drop your files here...' : dropText

  const activeBg = useColorModeValue('gray.100', 'gray.600')
  const borderColor = useColorModeValue(isDragActive ? 'teal.300' : 'gray.300', isDragActive ? 'teal.500' : 'gray.500')

  return (
    <Center
      flexDirection="column"
      p={8}
      cursor="pointer"
      bg={isDragActive ? activeBg : 'transparent'}
      _hover={{ bg: activeBg }}
      transition="background-color 0.2s ease"
      borderRadius={4}
      border="3px dashed"
      borderColor={borderColor}
      {...getRootProps()}
    >
      <input {...getInputProps()} />
      <Icon w={8} h={8} as={RiFile3Line} mb={4} />
      <p>{text}</p>
    </Center>
  )
}

export default Dropzone
