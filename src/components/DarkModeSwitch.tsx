import { useColorMode, Button } from '@chakra-ui/react'
import { MoonIcon, SunIcon } from '@chakra-ui/icons'

const DarkModeSwitch = () => {
  const { colorMode, toggleColorMode } = useColorMode()
  const isDark = colorMode === 'dark'
  return (
    <Button color="gray.600" onClick={toggleColorMode} p={0} variant="ghost">
      {isDark ? <SunIcon /> : <MoonIcon />}
    </Button>
  )
}

export default DarkModeSwitch
