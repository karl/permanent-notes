import { useState } from 'react'
import {
  Flex,
  Text,
  Icon,
  Button,
  ButtonGroup,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverBody,
  Box,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { RiAccountCircleFill, RiLogoutCircleRLine } from 'react-icons/ri'
import { useSessionStorageValue } from '@react-hookz/web'
import { useAsync } from 'react-use'

import DarkModeSwitch from './DarkModeSwitch'
import { getWalletAddress, getWalletBalance, WalletKey } from 'services/arweave'

const AccountPopover = ({
  address,
  balance,
  onLogout,
}: {
  address: string | null
  balance: string | null
  onLogout: () => void
}) => {
  const LogoutButton = () => (
    <Button color="gray.600" p={0} variant="ghost">
      <Icon w={6} h={6} as={RiLogoutCircleRLine} onClick={onLogout} />
    </Button>
  )

  const UserContent = () => (
    <Flex>
      <Box>
        <Text>{address}</Text>
        <Text color="red.500" mt={2}>
          {balance} AR
        </Text>
      </Box>
      <LogoutButton />
    </Flex>
  )

  return (
    <Popover placement="top-end">
      <PopoverTrigger>
        <Button color="gray.600" p={0} variant="ghost">
          <Icon w={6} h={6} as={RiAccountCircleFill} />
        </Button>
      </PopoverTrigger>
      <PopoverContent>
        <PopoverArrow />
        <PopoverBody>{address ? <UserContent /> : <Text>Loading</Text>}</PopoverBody>
      </PopoverContent>
    </Popover>
  )
}

const Header = () => {
  const router = useRouter()
  const [walletKey] = useSessionStorageValue<WalletKey>('wallet-key')
  const [walletAddress, setWalletAddress] = useState<string>()
  const [walletBalance, setWalletBalance] = useState<string>()

  useAsync(async () => {
    console.log('async', walletAddress)
    if (walletAddress === '') {
      return
    }

    if (!walletAddress || !walletBalance) {
      const address = await getWalletAddress(walletKey as WalletKey)
      setWalletAddress(address)
      const balance = await getWalletBalance(address)
      setWalletBalance(balance)
    }
  }, [walletKey, walletAddress, walletBalance])

  const onLogout = () => {
    setWalletAddress('')
    sessionStorage.clear()
    router.push('/')
  }

  return (
    <Flex alignItems="center" justifyContent="space-between" as="header" p={2} w="100%">
      {walletAddress && walletBalance ? (
        <>
          <Text>Welcome</Text>
          <ButtonGroup>
            <DarkModeSwitch />
            <AccountPopover address={walletAddress} balance={walletBalance} onLogout={onLogout} />
          </ButtonGroup>
        </>
      ) : (
        <>
          <Text></Text>
          <ButtonGroup>
            <DarkModeSwitch />
          </ButtonGroup>
        </>
      )}
    </Flex>
  )
}

export default Header
