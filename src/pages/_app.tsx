import { ChakraProvider, Flex } from '@chakra-ui/react'

import theme from '../theme'
import { AppProps } from 'next/app'
import Head from 'next/head'
import Auth from 'components/Auth'
import DarkModeSwitch from 'components/DarkModeSwitch'
import Container from 'components/Container'
import Header from 'components/Header'
import Footer from 'components/Footer'

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Permanent Notes</title>
      </Head>
      <ChakraProvider resetCSS theme={theme}>
        <Auth>
          <Container h="100vh">
            <Header />
            <Flex flexGrow={1} justifyContent="center" w="100%">
              <Component {...pageProps} />
            </Flex>
            <Footer />
          </Container>
        </Auth>
      </ChakraProvider>
    </>
  )
}

export default App
