import {
  Button,
  Flex,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  useDisclosure,
  VStack,
} from '@chakra-ui/react'
import { useSessionStorageValue } from '@react-hookz/web'
import { useRouter } from 'next/router'
import { WalletKey } from 'services/arweave'

import Container from 'components/Container'
import Dropzone from 'components/Dropzone'
import Footer from 'components/Footer'
import Logo from 'components/Logo'
import LogoSquare from 'components/LogoSquare'

const Index = () => {
  const router = useRouter()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [walletKey, setWalletKey] = useSessionStorageValue('wallet-key')

  if (walletKey) {
    router.push('/home')
  }

  const saveWallet = (wallet: File[]) => {
    const reader = new FileReader()

    reader.readAsText(wallet[0])
    reader.onload = () => {
      const walletText = reader.result as string
      setWalletKey(JSON.parse(walletText) as WalletKey)
    }
  }

  return (
    <>
      <Flex flexDirection="column" justifyContent="center" maxW="30rem" w={{ base: '80vw', md: '35vw' }}>
        <Logo />
        <VStack mt={{ base: 12, md: 24 }}>
          <Button onClick={onOpen}>Connect your wallet!</Button>
          <Link href="https://faucet.arweave.net" isExternal>
            Don&apos;t have a wallet? Get one here!
          </Link>
        </VStack>
      </Flex>

      <Modal closeOnOverlayClick={false} isCentered isOpen={isOpen} motionPreset="slideInBottom" onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalCloseButton />
          <ModalBody pb={6} pt={12}>
            <Dropzone
              dropText="Drag 'n' drop your wallet here, or click to select files"
              fileType=".json"
              onFileAccepted={saveWallet}
            />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}

export default Index
