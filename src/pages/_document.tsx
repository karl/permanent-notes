import NextDocument, { Html, Head, Main, NextScript } from 'next/document'
import { ColorModeScript } from '@chakra-ui/react'

export default class Document extends NextDocument {
  render() {
    return (
      <Html>
        <Head>
          <link rel="icon" href="/favicon.ico" sizes="any"></link>
          <link rel="icon" href="/favicon.svg" type="image/svg+xml"></link>
          <link rel="apple-touch-icon" href="/apple-touch-icon.png"></link>
          <link rel="manifest" href="/site.webmanifest"></link>
        </Head>
        <body>
          <ColorModeScript initialColorMode="system" />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
