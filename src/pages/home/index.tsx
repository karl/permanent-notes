import { useEffect, useState } from 'react'
import {
  Button,
  Flex,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Text,
  useDisclosure,
  VStack,
} from '@chakra-ui/react'
import { useSessionStorageValue } from '@react-hookz/web'
import { useAsync } from 'react-use'

import Container from 'components/Container'
import DarkModeSwitch from 'components/DarkModeSwitch'
import Footer from 'components/Footer'
import Logo from 'components/Logo'
import LogoSquare from 'components/LogoSquare'

import { getWalletAddress, getWalletBalance, WalletKey } from 'services/arweave'

const Home = () => {
  // const [walletKey] = useSessionStorageValue<WalletKey>('wallet-key')
  // const [walletAddress, setWalletAddress] = useSessionStorageValue<string>('wallet-address')
  // const [walletBalance, setWalletBalance] = useSessionStorageValue<string>('wallet-balance')

  // useAsync(async () => {
  //   if (!walletAddress || !walletBalance) {
  //     const address = await getWalletAddress(walletKey as WalletKey)
  //     setWalletAddress(address)
  //     const balance = await getWalletBalance(address)
  //     setWalletBalance(balance)
  //   }
  // }, [])

  return (
    <Flex flexDirection="column" justifyContent="center">
      <Text>Home</Text>
    </Flex>
  )
}

export default Home
