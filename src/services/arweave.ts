import Arweave from 'arweave'

export type WalletKey = {
  kty: string
  n: string
  e: string
  d: string
  p: string
  q: string
  dp: string
  dq: string
  qi: string
}

// const arweave = Arweave.init({
//   host: '127.0.0.1',
//   port: 1984,
//   protocol: 'http',
// })

const arweave = Arweave.init()

export const getWalletAddress = async (walletKey: WalletKey): Promise<string> =>
  await arweave.wallets.jwkToAddress(walletKey)

export const getWalletBalance = async (walletAddress: string) => {
  const balance = await arweave.wallets.getBalance(walletAddress)
  return arweave.ar.winstonToAr(balance)
}
